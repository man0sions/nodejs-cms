var express = require('express');
var router = express.Router();
var Article = require('../SeModel/Article');
var ArticleCat = require('../SeModel/ArticleCat');
var Entities = require('html-entities').XmlEntities;



/* GET home page. */
router.get('/logout', function(req, res, next) {
  req.session.user = {};
  res.redirect("/");

});
//首页
router.get('/', function(req, res, next) {

    Article.paginate({catid:'5678c61aef3f0d36210c0348'},{page:req.query.page, limit:12}, function(err, data, total) {

        if(req.query.ajax)
        {
            return res.json(data);
        }
        var pager = Main.stePager(req,total);
        var ret = Main.showImages(data);

        res.render('web/default/index',{arts:ret,pager:pager,title:res.locals.title});


    });

});
//文章分类
router.get('/articleCat', function(req, res, next) {
    var catid = req.query.catid ;
    var condition = catid ? {catid:catid}:{};

    Article.find(condition,function(err,data){
        if(req.query.ajax)
        {
            return res.json(data);
        }
        res.render('web/article/index',{title:'文章列表',data:data});
    });

});
//文章内页
router.get('/article/view', function(req, res, next) {
    var id = req.query.id ;

    Article.find({_id:id},function(err,data){
        var data = data[0];
        if(req.query.ajax)
        {
            return res.json(data);
        }
        entities = new Entities();

        data.content = entities.decode(data.content);
        console.log(data.content);
        res.render('web/article/view',{title:data.name,data:data});
    });

});


module.exports = router;
