var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var mongoosePaginate = require('mongoose-paginate');


var Schema = mongoose.Schema;

var Config = new Schema({
    name: { type: String, required: true },
    content: { type: String},
    author: { type: String, required: true },
    createtime: { type: Date, required: true, default: Date.now  },
    updatetime: { type: Date, default: Date.now  }

});
Config.plugin(mongoosePaginate);

module.exports = db.model('Config', Config);

