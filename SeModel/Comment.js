var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var mongoosePaginate = require('mongoose-paginate');


var Schema = mongoose.Schema;

var Comment = new Schema({
    name: { type: String, required: true },
    article_id: { type: String ,required:true},
    content: { type: String},
    createtime: { type: Date, required: true, default: Date.now  },
    updatetime: { type: Date, default: Date.now  }

});
Comment.plugin(mongoosePaginate);

module.exports = db.model('Comment', Comment);

