var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var mongoosePaginate = require('mongoose-paginate');



var Article = new mongoose.Schema({
    name: { type: String, required: true },
    image: { type: String },
    imgs: { type: Array },
    catid: { type: String ,required:true},
    content: { type: String},
    author: { type: String, required: true },
    sort:{ type: Number,default:100 },
    createtime: { type: Date, required: true, default: Date.now  },
    updatetime: { type: Date, default: Date.now  }

});
Article.plugin(mongoosePaginate);



var Model = db.model('Article', Article); // Model.paginate()


module.exports = Model;

